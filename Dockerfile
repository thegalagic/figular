# SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# figular generates visualisations from flexible, reusable parts
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

FROM docker.io/tiangolo/uvicorn-gunicorn-fastapi:python3.9-slim-2021-10-02

ENV PORT 8080
EXPOSE 8080

RUN apt-get update \
    && apt-get install -y asymptote=2.69+ds-1 \
                          texlive-fonts-recommended=2020.20210202-3 \
                          texlive-xetex \
    && rm -rf /var/lib/apt/lists/*

COPY ./figular/app/requirements.txt /app/app/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /app/app/requirements.txt

COPY ./figular/ /app/figular
COPY ./figular/app/ /app/app
