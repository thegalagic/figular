---
widget: "figular"
headless: true
active: true
weight: 10
title: Circle
help: Enter concepts below to illustrate in a circle.
help_link: https://gitlab.com/thegalagic/figular/-/blob/main/docs/figures/concept/circle.md
data_area_id: concepts
data_area_label: Concepts
figure_url: concept/circle
example_content: |-
  Democracy
  Freedom
  Inclusiveness
  Membership
  Consent
  Voting
  Right to life
example_media: 'media/example.png'
design:
  columns: '1'
---
