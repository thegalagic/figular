---
widget: "figular"
headless: true
active: true
weight: 10
title: Org Chart
help: Copy/paste from a spreadsheet or enter your organisation structure below.
help_link: https://gitlab.com/thegalagic/figular/-/blob/main/docs/figures/org/orgchart.md
data_area_id: rolesandreporting
data_area_label: Enter roles and reporting
figure_url: org/orgchart
example_content: |-
  Role|Description|Reports To
  CEO|Chief Executive Officer|
  CTO|Chief Technology Officer|CEO
  COO|Chief Operations Officer|CEO
  CFO|Chief Financial Officer|CEO
  CMO|Chief Marketing Officer|CEO
example_media: 'media/example.png'
design:
  columns: '1'
---
