---
widget: "figular"
headless: true
active: true
weight: 10
title: Timeline
help: Enter dates and events to create a timeline.
help_link: https://gitlab.com/thegalagic/figular/-/blob/main/docs/figures/time/timeline.md
data_area_id: timeline
data_area_label: Enter dates and text
figure_url: time/timeline
example_content: |-
  2022/01/01|Jan|Nothing happened
  2022/03/01|March|Something happened
  2022/04/01|April|Nothing happened
example_media: 'media/example.png'
design:
  columns: '1'
---
