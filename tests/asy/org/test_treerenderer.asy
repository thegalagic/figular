// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import "figular/styledom" as styledom;
import "org/orgchart.asy" as orgchart;

page drawpic(real width=100) {
  page p;
  real height = 100;
  shape(p,
        shift(-width/2, -height/2)*scale(width, height)*unitsquare,
        style(border_width=0, background_color=red));
  return p;
}

/* *********************************
   * TESTS                         *
   ********************************* */

void test_justahead() {
  treerenderer tr = treerenderer(drawpic());

  add(currentpage, tr.render());
}

// Straight line between them
void test_headandleaf() {
  treerenderer tr = treerenderer(drawpic());
  tr.addleaf(drawpic());

  add(currentpage, tr.render());
}

// Head and two below
void test_headandtwoleaves() {
  treerenderer tr = treerenderer(drawpic());
  tr.addleaf(drawpic());
  tr.addleaf(drawpic());

  add(currentpage, tr.render());
}

// Subtree placed directly below the head to preserve width
void test_subtree_justone() {
  treerenderer tr = treerenderer(drawpic());

  treerenderer subtree = treerenderer(drawpic());
  subtree.addleaf(drawpic());
  subtree.addleaf(drawpic());
  tr.addtree(subtree);

  add(currentpage, tr.render());
}

// Do put a leaf and lone subtree together as they will
// save vertical space and cost no extra width
void test_oneleafandasubtree() {
  treerenderer tr = treerenderer(drawpic());

  treerenderer subtree = treerenderer(drawpic());
  subtree.addleaf(drawpic());
  subtree.addleaf(drawpic());
  tr.addleaf(drawpic());
  tr.addtree(subtree);

  add(currentpage, tr.render());
}

// Don't place a lone subtree with a leaf on the same level
// as it blows out the width unnecessarily
void test_twoleavesandasubtree() {
  treerenderer tr = treerenderer(drawpic());

  treerenderer subtree = treerenderer(drawpic());
  subtree.addleaf(drawpic());
  subtree.addleaf(drawpic());
  tr.addleaf(drawpic());
  tr.addleaf(drawpic());
  tr.addtree(subtree);

  add(currentpage, tr.render());
}

// Subtrees are nicely packed next to each other
void test_subtree_tessalation() {
  treerenderer tr = treerenderer(drawpic());

  treerenderer subtree1 = treerenderer(drawpic());
  subtree1.addleaf(drawpic());

  treerenderer subtree2 = treerenderer(drawpic());
  subtree2.addleaf(drawpic());
  subtree2.addleaf(drawpic());

  tr.addtree(subtree1);
  tr.addtree(subtree2);

  add(currentpage, tr.render());
}

// Subtress of different depths are still nicely packed
void test_subtree_tessalation_diffdepths() {
  treerenderer tr = treerenderer(drawpic());

  treerenderer subtree1 = treerenderer(drawpic());
  subtree1.addleaf(drawpic());
  subtree1.addleaf(drawpic());

  treerenderer subtree2 = treerenderer(drawpic());
  subtree2.addleaf(drawpic());
  subtree2.addleaf(drawpic());
  subtree2.addtree(treerenderer(drawpic()));
  subtree2.addtree(treerenderer(drawpic()));
  subtree2.addtree(treerenderer(drawpic()));

  tr.addtree(subtree1);
  tr.addtree(subtree2);

  add(currentpage, tr.render());
}
