// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import "org/orgchart" as orgchart;

void check_person(organisation org, string name, string title) {
  person a = org.lookup(name);
  assert(a != person.nullperson);
  assert(a.name == name);
  assert(a.title == title);
}

void check_boss(organisation org, string boss_name, string employee_name) {
  person boss = org.lookup(boss_name);
  assert(boss != person.nullperson);
  person employee = org.lookup(employee_name);
  assert(employee != person.nullperson);

  bool found = false;
  for(person p : boss.team) {
    found = found || p == employee;
  }
  assert(found, boss_name + " is not the boss of " + employee_name);
}

void check_topdog(organisation org, string name) {
  assert(org.topdog == org.lookup(name), name + " is not topdog");
}

/* ******************************* 
   * TESTS                       *
   ******************************* */

void test_no_person() {
  organisation org;
  assert(org.lookup("Name") == person.nullperson);
  assert(org.topdog == null); 
}

void test_one_person() {
  organisation org;
  org.add("Name", "Title");
  check_person(org, "Name", "Title");
  check_topdog(org, "Name");
}

void test_two_people() {
  organisation org;
  org.add("A", "A Title");
  org.add("B", "B Title");
  check_person(org, "A", "A Title");
  check_person(org, "B", "B Title");
  // Topdog is always last bossless person seen
  check_topdog(org, "B");
}

void test_one_boss() {
  organisation org;
  org.add("A", "A Title");
  org.add("B", "B Title", "A");
  check_person(org, "A", "A Title");
  check_person(org, "B", "B Title");
  check_boss(org, "A", "B");
  check_topdog(org, "A");
}

void test_one_boss_reverse_order() {
  organisation org;
  org.add("B", "B Title", "A");
  org.add("A", "A Title");
  check_person(org, "A", "A Title");
  check_person(org, "B", "B Title");
  check_boss(org, "A", "B");
  check_topdog(org, "A");
}

void test_boss_never_explicitly_added() {
  organisation org;
  org.add("Marie Curie", "Manager", "Thomas Edison");
  org.add("Blaise Pascal", "Manager", "Thomas Edison");
  check_topdog(org, "Thomas Edison");
}

void test_realistic() {
  organisation org;
  org.add("Marie Curie", "Manager", "Thomas Edison");
  org.add("Blaise Pascal", "Manager", "Thomas Edison");
  org.add("Isaac Newton", "Manager", "Thomas Edison");
  org.add("Will Thomson", "Manager", "Marie Curie");
  org.add("Carl Gauss", "Manager", "Marie Curie");
  org.add("James Watt", "Manager", "Blaise Pascal");
  org.add("James Clerk Maxwell", "Manager", "Isaac Newton");
  org.add("Paul Dirac", "Manager", "Isaac Newton");
  check_boss(org, "Marie Curie", "Will Thomson");
  check_boss(org, "Marie Curie", "Carl Gauss");
  check_boss(org, "Blaise Pascal", "James Watt");
  check_boss(org, "Isaac Newton", "James Clerk Maxwell");
  check_boss(org, "Isaac Newton", "Paul Dirac");
  check_topdog(org, "Thomas Edison");
}
