// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import "org/orgchart" as orgchart;

struct mockorganisation {
  struct mockperson {
    string name;
    string title;
    string boss;
  }

  restricted organisation org;
  mockperson[] people;

  void add(string name, string title, string boss="") {
    mockperson person;
    person.name = name;
    person.title = title;
    person.boss = boss;
    people.push(person);
  }

  person lookup(string name) {
    return null;
  }

  void operator init() {
    this.org.add = this.add;
    this.org.lookup = this.lookup;
  }
}

organisation operator cast(mockorganisation mo) {
  return mo.org;
}

/******************************************************************************

TESTS

******************************************************************************/

string test_filename = "testdata_processinput";

void test_processinput_minimum() {
  mockorganisation mockorg = mockorganisation();

  string[] testdata = new string[] { "", '\r' };
  for(var data: testdata) {
    processinput(new string[] {data}, mockorg);
    assert(mockorg.people.length == 0);
  }
}

void test_processinput_justaname() {
  mockorganisation mockorg = mockorganisation();
  string[] testdata = new string[] { "Name" };

  processinput(testdata, mockorg);

  assert(mockorg.people.length == 1);
  assert(mockorg.people[0].name == "Name");
  assert(mockorg.people[0].title == "");
  assert(mockorg.people[0].boss == "");
}

void test_processinput_justnameanddesc() {
  mockorganisation mockorg = mockorganisation();
  string[] testdata = new string[] { "Name|Desc" };

  processinput(testdata, mockorg);

  assert(mockorg.people.length == 1);
  assert(mockorg.people[0].name == "Name");
  assert(mockorg.people[0].title == "Desc");
  assert(mockorg.people[0].boss == "");
}

void test_processinput_normal() {
  mockorganisation mockorg = mockorganisation();
  string[] testdata = new string[] { "Mcintyre Kapoor|Caretaker|Boss" };

  processinput(testdata, mockorg);

  assert(mockorg.people.length == 1);
  assert(mockorg.people[0].name == "Mcintyre Kapoor");
  assert(mockorg.people[0].title == "Caretaker");
  assert(mockorg.people[0].boss == "Boss");
}

void test_processinput_noboss() {
  mockorganisation mockorg = mockorganisation();
  string[] testdata = new string[] { "Mcintyre Kapoor|Caretaker" };

  processinput(testdata, mockorg);

  assert(mockorg.people.length == 1);
  assert(mockorg.people[0].name == "Mcintyre Kapoor");
  assert(mockorg.people[0].title == "Caretaker");
  assert(mockorg.people[0].boss == "");
}

void test_processinput_nobosswithsep() {
  mockorganisation mockorg = mockorganisation();
  string[] testdata = new string[] { "Mcintyre Kapoor|Caretaker|" };

  processinput(testdata, mockorg);

  assert(mockorg.people.length == 1);
  assert(mockorg.people[0].name == "Mcintyre Kapoor");
  assert(mockorg.people[0].title == "Caretaker");
  assert(mockorg.people[0].boss == "");
}

void test_processinput_loadsofsep() {
  mockorganisation mockorg = mockorganisation();
  string[] testdata = new string[] { "Mc|intyre|Kapoor|Care|taker||||" };

  processinput(testdata, mockorg);

  assert(mockorg.people.length == 1);
  assert(mockorg.people[0].name == "Mc");
  assert(mockorg.people[0].title == "intyre");
  assert(mockorg.people[0].boss == "Kapoor");
}

/******************************************************************************

Symbols

"The Comprehensive LaTeX Symbol List is your friend"
[symbols - How does one insert a backslash or a tilde (~) into LaTeX? - TeX - LaTeX Stack Exchange](https://tex.stackexchange.com/questions/9363/how-does-one-insert-a-backslash-or-a-tilde-into-latex)
Tests here follow the tables in the symbol list.
Note this is unfinished. We are defending by the API specs initially but longer
term we should be able to cope with any input.

******************************************************************************/

void test_processinput_latexescapablechars() {
  mockorganisation mockorg = mockorganisation();
  string[] testdata = new string[] { "& % $ # _ { }|& % $ # _ { }|& % $ # _ { }" };

  processinput(testdata, mockorg);

  assert(mockorg.people.length == 1);
  assert(mockorg.people[0].name == "\& \% \$ \# \_ \{ \}");
  assert(mockorg.people[0].title == "\& \% \$ \# \_ \{ \}");
  assert(mockorg.people[0].boss == "\& \% \$ \# \_ \{ \}");
}

void test_processinput_latextextmodecmds() {
  mockorganisation mockorg = mockorganisation();
  string[] testdata = new string[] { "\ ^ ~|\ ^ ~|\ ^ ~" };

  processinput(testdata, mockorg);

  assert(mockorg.people.length == 1);
  assert(mockorg.people[0].name == "\textbackslash{} \textasciicircum{} \textasciitilde{}");
  assert(mockorg.people[0].title == "\textbackslash{} \textasciicircum{} \textasciitilde{}");
  assert(mockorg.people[0].boss == "\textbackslash{} \textasciicircum{} \textasciitilde{}");
}
