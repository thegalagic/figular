// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import "figular/styledom" as styledom;
import "org/orgchart" as orgchart;

void test_integration() {
  string lines[] = {
                    "Mcintyre Kapoor|Caretaker|",
                    "Meakin Benfield|Caretaker|Mcintyre Kapoor",
                    "Bone Timberlake|Caretaker|Meakin Benfield",
                    "Hall Golding|Ball Fetcher|Mcintyre Kapoor",
                    "Yasmin Otoole|Teaching Assistant|Bone Timberlake",
                    "Kirkbright Higginson|Key Master|Hall Golding",
                    "Lockhart Yetman|Student|Yasmin Otoole",
                    "Oatridge Binnington|Key Master|Hall Golding",
                    "Rooney Hanlon|Deputy Head|Hall Golding",
                    "Awad Montgomery|Deputy Head|Oatridge Binnington",
                    "Olson Widdowson|Teaching Assistant|Hall Golding",
                    "Davies Frostick|Gatekeeper|Hall Golding",
                    "Sinclair Baldwin|Caretaker|Awad Montgomery",
                    "Johnstone Cornish|Deputy Head|Lockhart Yetman",
                    "Philip Towers|Ball Fetcher|Bone Timberlake",
                    "Elms Mcmanus|Caretaker|Davies Frostick",
                    "Skelton Riddell|Teaching Assistant|Elms Mcmanus",
                    "Thornley Dawes|Gatekeeper|Elms Mcmanus",
                    "Keilty Jubb|Headteacher|Olson Widdowson",
                    "Yeadon Regent|Headteacher|Johnstone Cornish",
                    "Ramsay Greene|Ball Fetcher|Johnstone Cornish",
                    "Barber Frey|Key Master|Ramsay Greene",
                    "Drake Kellie|Caretaker|Bone Timberlake",
                    "Bentley Turland|Coach|Olson Widdowson",
                    "Cowan Kirkpatrick|Deputy Head|Elms Mcmanus",
                    "Dodd Cockle|Key Master|Ramsay Greene",
                    "Thonon O keefe|Key Master|Philip Towers",
                    "Gladwell Kilminster|Student|Mcintyre Kapoor",
                    "Macarthur Rushby|Headteacher|Cowan Kirkpatrick",
                    "Riley Okeefe|Gatekeeper|Thonon O keefe",
                    "Doran Keywood|Deputy Head|Yasmin Otoole",
                    "Houston Fallows|Caretaker|Yasmin Otoole",
                    "Batty Elvin|Student|Awad Montgomery",
                    "Ewin Openshaw|Teaching Assistant|Meakin Benfield",
                    "O sullivan Ivory|Gatekeeper|Lockhart Yetman",
                    "Keegan Traynor|Deputy Head|Thornley Dawes",
                    "Newman Kerrighen|Caretaker|Macarthur Rushby",
                    "Jennison Dalzell|Coach|Hall Golding"
                    };
  run(currentpicture, lines);
}
