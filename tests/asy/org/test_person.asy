// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import "org/orgchart" as orgchart;

struct mocktreerenderer {
  restricted treerenderer treerenderer;
  restricted bool rendered = false;
  restricted page[] leaves;
  restricted treerenderer[] trees;

  void addleaf(page card) {
    this.leaves.push(card);
  }

  void addtree(treerenderer card) {
    this.trees.push(card);
  }

  page render() {
    this.rendered = true;
    return null;
  }

  void operator init(page head) {
    this.treerenderer.render = this.render;
    this.treerenderer.addleaf = this.addleaf;
    this.treerenderer.addtree = this.addtree;
  }
}

treerenderer operator cast(mocktreerenderer mtl) {
  return mtl.treerenderer;
}

mocktreerenderer[] mocktreerenderer;
int drawncards;

treerenderer treerendererfactory(page head) {
  mocktreerenderer mtr = mocktreerenderer(head);
  mocktreerenderer.push(mtr);
  return mtr;
}

page drawcard(string, string) {
  ++drawncards;
  return new page;
}

void test_setup() {
  mocktreerenderer.delete();
  drawncards = 0;
}

/* *********************************
   * TESTS                         *
   ********************************* */

void test_person_not_drawn() {
  test_setup();

  person p = person("Name", "Title");

  assert(mocktreerenderer.length == 0);
}

void test_person_draws_their_card() {
  test_setup();
  person p = person("Name", "Title");

  page result = p.draw(drawcard, treerendererfactory);

  assert(drawncards == 1);
  assert(mocktreerenderer.length == 1);
  assert(mocktreerenderer[0].rendered);
  assert(mocktreerenderer[0].leaves.length == 0);
  assert(mocktreerenderer[0].trees.length == 0);
}

void test_person_one_report() {
  test_setup();
  person p = person("Name", "Title");
  p.addteammember(person("Name", "Title"));

  page result = p.draw(drawcard, treerendererfactory);

  assert(drawncards == 2);
  assert(mocktreerenderer.length == 1);
  assert(mocktreerenderer[0].rendered);
  assert(mocktreerenderer[0].leaves.length == 1);
  assert(mocktreerenderer[0].trees.length == 0);
}

void test_person_multiple_reports() {
  test_setup();
  person p = person("Name", "Title");
  p.addteammember(person("Name", "Title"));
  p.addteammember(person("Name", "Title"));
  p.addteammember(person("Name", "Title"));

  page result = p.draw(drawcard, treerendererfactory);

  assert(drawncards == 4);
  assert(mocktreerenderer.length == 1);
  assert(mocktreerenderer[0].rendered);
  assert(mocktreerenderer[0].leaves.length == 3);
  assert(mocktreerenderer[0].trees.length == 0);
}

void test_person_reports_have_reports() {
  test_setup();
  person p1 = person("Name", "Title");
  person p2 = person("Name", "Title");
  p1.addteammember(p2);
  p1.addteammember(person("Name", "Title"));
  p2.addteammember(person("Name", "Title"));

  page result = p1.draw(drawcard, treerendererfactory);

  assert(drawncards == 4);
  assert(mocktreerenderer.length == 2);
  assert(mocktreerenderer[0].rendered);
  assert(mocktreerenderer[0].leaves.length == 1);
  assert(mocktreerenderer[0].trees.length == 1);
  assert(mocktreerenderer[1].leaves.length == 1);
  assert(mocktreerenderer[1].trees.length == 0);
}
