// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import "time/timeline" as itimeline;

/* *********************************
   * INPUT TESTS
   ********************************* */

void test_nothing() {
  datedentry[] de = itimeline.normalise(new string[]{});
  assert(de.length == 0);
}

void test_meaningless_whitespace() {
  datedentry[] de1 = itimeline.normalise(new string[]{'\r'});
  datedentry[] de2 = itimeline.normalise(new string[]{'\r|\r'});
  datedentry[] de3 = itimeline.normalise(new string[]{'\r|\r|\r'});
  assert(de1.length == 0);
  assert(de2.length == 0);
  assert(de3.length == 0);
}

void test_one_date() {
  string datetext = "1970/01/01";
  datedentry[] de = itimeline.normalise(new string[]{datetext});

  assert(de.length == 1);
  assert(de[0].d == parsedate(datetext));
  assert(de[0].title == "");
  assert(de[0].entry == "");
}

void test_bad_date_treated_as_entry() {
  string datetext = "2022/02/33";
  datedentry[] de = itimeline.normalise(new string[]{datetext});

  assert(de.length == 1);
  assert(de[0].d == nulldate);
  assert(de[0].title == "");
  assert(de[0].entry == datetext);
}

void test_too_many_fields() {
  datedentry[] de = itimeline.normalise(new string[]{"a|b|c|d|e|f"});

  assert(de.length == 1);
  assert(de[0].d == nulldate);
  assert(de[0].title == "a");
  assert(de[0].entry == "b");
}

void test_too_many_fields_valid_date() {
  string datetext = "2001/01/01";
  datedentry[] de = itimeline.normalise(new string[]{datetext + "|a|b|c|d|e|f"});

  assert(de.length == 1);
  assert(de[0].d == parsedate(datetext));
  assert(de[0].title == "a");
  assert(de[0].entry == "b");
}

void test_tricky_chars() {
  string[] testdata = new string[] { "& % $ # _ { }|& % $ # _ { }|& % $ # _ { }" };
  datedentry[] de = itimeline.normalise(testdata);

  assert(de.length == 1);
  assert(de[0].d == nulldate);
  assert(de[0].title == "\& \% \$ \# \_ \{ \}");
  assert(de[0].entry == "\& \% \$ \# \_ \{ \}");
}

void test_long_fields() {
  string longtext = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa|aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa|aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
  datedentry[] de = itimeline.normalise(new string[]{longtext});

  assert(de.length == 1);
  assert(de[0].d == nulldate);
  assert(de[0].title == "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
  assert(de[0].entry == "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
}

void test_multiple_lines() {
  int numentries = 10;
  string[] input = new string[]{};

  for(int i = 0; i < numentries ; ++i) {
    input.push(time(i*100000, format="%Y/%m/%d") + "|Title" + (string)i + "|Entry" + (string)i);
  }

  datedentry[] de = itimeline.normalise(input);

  assert(de.length == numentries);
  for(int i = 0 ; i < numentries ; ++i) {
    assert(de[i].d == parsedate(time(i*100000, format="%Y/%m/%d")));
    assert(de[i].title == "Title" + (string)i);
    assert(de[i].entry == "Entry" + (string)i);
  }
}

void test_non_ascending_dates_ignored() {
  string[] input = new string[]{};
  input.push("1970/01/01|Entry");
  input.push("1969/01/01|Entry");
  input.push("1971/01/01|Entry");

  datedentry[] de = itimeline.normalise(input);

  assert(de.length == 3);
  assert(de[0].d == parsedate("1970/01/01"));
  // Non-ascending dates are treated as if nulldate
  assert(de[1].d == nulldate);
  assert(de[1].entry == "Entry");
  assert(de[2].d == parsedate("1971/01/01"));
}

/* *********************************
   * GAPPER
   ********************************* */

void test_gap_nothing() {
  datedentry[] de = new datedentry[]{};
  gapper g = gapper(de, 100);

  assert(g.gaps().length == 0);
}

void test_gap_one_date() {
  datedentry a = datedentry(date(0), "Title", "Entry");
  datedentry[] de = new datedentry[]{a};
  gapper g = gapper(de, 100);;

  assert(g.gaps().length == 1);
  assert(g.gaps()[0] == 0);
}

void test_gap_one_no_date() {
  datedentry a = datedentry(nulldate, "Title", "Entry");
  datedentry[] de = new datedentry[]{a};
  gapper g = gapper(de, 100);;

  assert(g.gaps().length == 1);
  assert(g.gaps()[0] == 0);
}

void test_gap_one_no_date() {
  datedentry a = datedentry(nulldate, "Title", "Entry");
  datedentry[] de = new datedentry[]{a};
  gapper g = gapper(de, 100);;

  assert(g.gaps().length == 1);
  assert(g.gaps()[0] == 0);
}

void test_gap_multi_dates_one_date() {
  datedentry a = datedentry(nulldate, "Title", "Entry");
  datedentry b = datedentry(date(0), "Title", "Entry");
  datedentry c = datedentry(nulldate, "Title", "Entry");
  datedentry[] de = new datedentry[]{a, b, c};
  gapper g = gapper(de, 100);

  assert(g.gaps().length == 3);
  assert(g.gaps()[0] == 0);
  assert(g.gaps()[1] == 100);
  assert(g.gaps()[2] == 200);
}

void test_gap_multi_dates_all_dates() {
  datedentry a = datedentry(parsedate("1970/01/01"), "Title", "Entry");
  datedentry b = datedentry(parsedate("1970/01/02"), "Title", "Entry");
  datedentry c = datedentry(parsedate("1970/01/04"), "Title", "Entry");
  datedentry[] de = new datedentry[]{a, b, c};
  gapper g = gapper(de, 100);

  assert(g.gaps().length == 3);
  assert(g.gaps()[0] == 0);
  assert(g.gaps()[1] == 100);
  assert(g.gaps()[2] == 300);
}

void test_gap_multi_dates_one_undated() {
  datedentry[] de = new datedentry[]{
    datedentry(parsedate("1970/01/01"), "Title", "Entry"),
    datedentry(nulldate, "Title", "Entry"),
    datedentry(parsedate("1970/01/02"), "Title", "Entry"),
    };
  gapper g = gapper(de, 100);

  assert(g.gaps().length == 3);
  assert(g.gaps()[0] == 0);
  assert(g.gaps()[1] == 100);
  assert(g.gaps()[2] == 200);
}

void test_gap_multi_dates_two_undated() {
  datedentry[] de = new datedentry[]{
    datedentry(parsedate("1970/01/01"), "Title", "Entry"),
    datedentry(nulldate, "Title", "Entry"),
    datedentry(parsedate("1970/01/03"), "Title", "Entry"),
    datedentry(nulldate, "Title", "Entry"),
    datedentry(parsedate("1970/01/07"), "Title", "Entry"),
    };
  gapper g = gapper(de, 100);

  assert(g.gaps().length == 5);
  assert(g.gaps()[0] == 0);
  assert(g.gaps()[1] == 100);
  assert(g.gaps()[2] == 200);
  assert(g.gaps()[3] == 400);
  assert(g.gaps()[4] == 600);
}

void test_gap_multi_dates_multiple_undated() {
  datedentry[] de = new datedentry[]{
    datedentry(parsedate("1970/01/01"), "Title", "Entry"),
    datedentry(nulldate, "Title", "Entry"),
    datedentry(nulldate, "Title", "Entry"),
    datedentry(nulldate, "Title", "Entry"),
    datedentry(nulldate, "Title", "Entry"),
    datedentry(parsedate("1970/01/03"), "Title", "Entry"),
    };
  gapper g = gapper(de, 100);

  assert(g.gaps().length == 6);
  assert(g.gaps()[0] == 0);
  assert(g.gaps()[1] == 100);
  assert(g.gaps()[2] == 200);
  assert(g.gaps()[3] == 300);
  assert(g.gaps()[4] == 400);
  assert(g.gaps()[5] == 500);
}

void test_gap_multi_dates_multiple_undated_start() {
  datedentry[] de = new datedentry[]{
    datedentry(nulldate, "Title", "Entry"),
    datedentry(nulldate, "Title", "Entry"),
    datedentry(parsedate("1970/01/01"), "Title", "Entry"),
    datedentry(parsedate("1970/01/03"), "Title", "Entry"),
    };
  gapper g = gapper(de, 100);

  assert(g.gaps().length == 4);
  assert(g.gaps()[0] == 0);
  assert(g.gaps()[1] == 100);
  assert(g.gaps()[2] == 200);
  assert(g.gaps()[3] == 300);
}

void test_gap_multi_dates_multiple_undated_end() {
  datedentry[] de = new datedentry[]{
    datedentry(parsedate("1970/01/01"), "Title", "Entry"),
    datedentry(parsedate("1970/01/03"), "Title", "Entry"),
    datedentry(nulldate, "Title", "Entry"),
    datedentry(nulldate, "Title", "Entry"),
    };
  gapper g = gapper(de, 100);

  assert(g.gaps().length == 4);
  assert(g.gaps()[0] == 0);
  assert(g.gaps()[1] == 100);
  assert(g.gaps()[2] == 200);
  assert(g.gaps()[3] == 300);
}

/* *********************************
   * DRAW LABELS
   ********************************* */

void test_draw_labels_no_entries() {
  page p;
  drawlabels(p, new datedentry[]{}, 12pt);

  assert(p.empty());
}
