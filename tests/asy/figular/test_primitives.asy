// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

// Real dom required to test the primitives
import "figular/styledom" as styledom;

/* *********************************
   * TESTS                         *
   ********************************* */

void test_lasts() {
  textbox a1 = textbox();
  assert(textbox.lasttextbox == a1);
  textbox a2 = textbox();
  assert(textbox.lasttextbox == a2);
  assert(textbox.lasttextbox != a1);

  circle a1 = circle(r=10, center=(0,0));
  assert(circle.lastcircle == a1);
  circle a2 = circle(r=10, center=(0,0));
  assert(circle.lastcircle == a2);
  assert(circle.lastcircle != a1);

  shape a1 = shape(unitsquare);
  assert(shape.lastshape == a1);
  shape a2 = shape(unitsquare);
  assert(shape.lastshape == a2);
  assert(shape.lastshape != a1);

  line a1 = line(unitsquare);
  assert(line.lastline == a1);
  line a2 = line(unitsquare);
  assert(line.lastline == a2);
  assert(line.lastline != a1);
}

void test_scrub_on_change() {
  page p;
  circle c = circle(p, 10, (0,0));
  assert(p.drawnpaths.length == 1);
  c.change(r=5);
  assert(p.drawnpaths.length == 1);

  page p;
  shape s = shape(p, (0,0) -- (10, 10));
  assert(p.drawnpaths.length == 1);
  s.change(g=(10, 10) -- (20, 20));
  assert(p.drawnpaths.length == 1);

  page p;
  textbox t = textbox(p);
  assert(p.labels.length == 1);
  t.change();
  assert(p.labels.length == 1);

  page p;
  line l = line(p, unitsquare);
  assert(p.drawnpaths.length == 1);
  l.change(unitcircle);
  assert(p.drawnpaths.length == 1);
}

void test_size_consistant() {
  page p;
  textbox t = textbox(p, "hello");
  pair sizeafter, sizebefore = t.size();

  t.change(place=(100,100));
  sizeafter = t.size();

  assert(length(sizebefore) == length(sizeafter));
}
