// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et. al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import "figular/date" as idate;        

/* *********************************
   * TESTS                         *
   ********************************* */

void test_nulldate() {
  date d = nulldate;
  date e;
  date f = null;

  assert(d == nulldate);
  assert(e != nulldate);
  assert(d != e);
  assert(f != nulldate);
  assert(f != d);
  assert(f != e);
}

void test_comparisons() {
  date d = parsedate("2022/01/01");
  date e = parsedate("2022/01/02");
  date f = parsedate("2022/01/03");
  date g = parsedate("2022/01/03");

  assert(d < e);
  assert(d < f);
  assert(e < f);
  assert(f > e);
  assert(e > d);
  assert(g >= f);
  assert(f <= f);
  assert(f == g);
  assert(d != g);
}

void test_maths() {
  int secondsinaday = 60*60*24;
  date d = parsedate("2022/01/01");
  date e = parsedate("2022/01/02");
  date epoch = parsedate("1970/01/01");

  assert((d - d) == 0);
  assert((e - d) == secondsinaday);
  assert((d - e) == -secondsinaday);
  assert((d + date(60*60*24)) == e);
}

void test_casting() {
  int secondsinaday = 60*60*24;
  date d = date(secondsinaday);

  assert((int)(d) == secondsinaday);
  assert((string)(d) == time(secondsinaday));
}

void test_parsing_rubbish() {
  date[] dates;
  dates.push(parsedate("jfkdf"));
  dates.push(parsedate("ZZ/01/01"));
  dates.push(parsedate("2000/13/01"));
  dates.push(parsedate("2000/01/33"));
  dates.push(parsedate('\v'));
  dates.push(parsedate('\x0C'));
  dates.push(parsedate('@'));
  dates.push(parsedate('-2000/01/01'));

  for(date d: dates) {
    assert(d == nulldate);
  }
}

void test_parsing_crazydates() {
  date d = parsedate("0/01/01");
  assert(d != nulldate);
  d = parsedate("9999/01/01");
  assert(d != nulldate);
}
