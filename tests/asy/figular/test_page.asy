// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import "figular/page" as ipage;
import "figular/styledom" as istyledom;

/* *********************************
   * TESTS                         *
   ********************************* */

void test_size() {
  page p;
  assert(p.size() == (0,0));
  line l = line(p, (0,0) -- (10, 10));
  assert(p.size() == (11,11));

  page p;
  line l = line(p, (100,100) -- (110, 110));
  assert(p.size() == (11,11));

  // A label is the same size wherever it is
  page p;
  textbox t = textbox(p, "hello");
  pair sizebefore = p.size();
  page p;
  textbox t = textbox(p, "hello", place=(100, 100));
  // We use length as the sizes are just slightly different
  assert(length(p.size()) == length(sizebefore));
}
