// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import "figular/styledom" as styledom;


/* *********************************
   * TESTS                         *
   ********************************* */

void test_stylerule() {
  stylerule sr = stylerule(selectorlist(selector(elementtype.circle)), new style);
  assert(sr.match(selectorlist(selector(elementtype.circle, 1))));
  assert(!sr.match(selectorlist(selector(elementtype.shape, 1))));
}

void test_onestyle() {
  dom_styledom.clearrules();
  style redstyle = style(border_color=red);
  stylerule sr = stylerule(selectorlist(selector(elementtype.circle)), redstyle);
  dom_styledom.addrule(sr);

  style s = dom_styledom.addcircle();
  style l = dom_styledom.addline();

  assert(s == redstyle);
  assert(l != redstyle);
}

void test_two_separate_styles() {
  dom_styledom.clearrules();
  style redstyle = style(border_color=red);
  stylerule cr = stylerule(selectorlist(selector(elementtype.circle)), redstyle);
  stylerule lr = stylerule(selectorlist(selector(elementtype.line)), redstyle);
  dom_styledom.addrule(cr);
  dom_styledom.addrule(lr);

  style s = dom_styledom.addcircle();
  style l = dom_styledom.addline();
  style t = dom_styledom.addtextbox();

  assert(s == redstyle);
  assert(l == redstyle);
  assert(t != redstyle);
}

void test_two_styles_for_nth_children() {
  dom_styledom.clearrules();
  style redstyle = style(border_color=red);
  style bluestyle = style(background_color=blue);
  style combinedstyle = bluestyle + redstyle;
  stylerule ss1 = stylerule(selectorlist(selector(elementtype.circle)), redstyle);
  stylerule ss2 = stylerule(selectorlist(selector(elementtype.circle, 2)), bluestyle);
  dom_styledom.addrule(ss1);
  dom_styledom.addrule(ss2);

  style s1 = dom_styledom.addcircle();
  style s2 = dom_styledom.addcircle();

  assert(s1 == redstyle);
  assert(s2 == combinedstyle);
  assert(s1 != s2);
}
