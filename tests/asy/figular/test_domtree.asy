// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et. al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import "figular/styledom" as styledom;
import "figular/domtree" as domtree;
// This import below required to avoid an asy bug(?) "cannot convert style() to
// style()" where asy gets confused about types it appears. This import must appear
// after styledom (which also imports style). It's as if we have to refresh the def
// of style...
import "figular/style" as style;        

/* *********************************
   * MOCK                          *
   ********************************* */

struct mockstyledom {
  private domtree dt;
  private selectorlist sl;

  void nextparentisatextbox(){ dt.nextparentisatextbox(); }
  void nextparentisacircle(){ dt.nextparentisacircle(); }
  void nextparentisashape(){ dt.nextparentisashape(); }
  void nextparentisaline(){ dt.nextparentisaline(); }

  style addtextbox(){ this.sl = dt.addtextbox(); return new style; }
  style addcircle(){ this.sl = dt.addcircle(); return new style; }
  style addshape(){ this.sl = dt.addshape(); return new style; }
  style addline(){ this.sl = dt.addline(); return new style; }

  domtree getdomtree() {
    return this.dt;
  }

  selectorlist getselectorlist() {
    return this.sl;
  }
}

dom operator cast(mockstyledom sd) {
  dom result;
  result.nextparentisatextbox = sd.nextparentisatextbox;
  result.nextparentisacircle = sd.nextparentisacircle;
  result.nextparentisashape = sd.nextparentisashape;
  result.nextparentisaline = sd.nextparentisaline;
  result.addtextbox = sd.addtextbox;
  result.addcircle = sd.addcircle;
  result.addshape = sd.addshape;
  result.addline = sd.addline;
  return result;
}

// Create/update global, singleton dom(s)
// One available as its actual type
mockstyledom dom_mockstyledom = new mockstyledom;
// Update global singleton dom
dom = dom_mockstyledom;

/* *********************************
   * TESTS                         *
   ********************************* */

void test_basics() {
  textbox t = textbox();
  selectorlist st = dom_mockstyledom.getselectorlist();
  circle c = circle(50, (0,0));
  selectorlist sc = dom_mockstyledom.getselectorlist();
  shape s = shape(unitsquare);
  selectorlist ss = dom_mockstyledom.getselectorlist();
  line l = line(unitsquare);
  selectorlist sl = dom_mockstyledom.getselectorlist();

  assert(st.isselectedby(selectorlist(selector(elementtype.textbox, 1))));
  assert(sc.isselectedby(selectorlist(selector(elementtype.circle, 2))));
  assert(ss.isselectedby(selectorlist(selector(elementtype.shape, 3))));
  assert(sl.isselectedby(selectorlist(selector(elementtype.line, 4))));
}

void test_parents_onedeep() {
  circle c = circle(50, (0,0));
  selectorlist sc = dom_mockstyledom.getselectorlist();
  line l = line(unitsquare, c);
  selectorlist sl = dom_mockstyledom.getselectorlist();

  assert(sc.isselectedby(selectorlist(selector(elementtype.circle, 1))));
  assert(sl.isselectedby(selectorlist(selector(elementtype.circle, 1),
                                      selector(elementtype.line, 1))));
}

void test_parents_onedeep_secondchild() {
  circle c = circle(50, (0,0));
  selectorlist sc = dom_mockstyledom.getselectorlist();
  line(unitsquare, c);
  line l = line(unitsquare, c);
  selectorlist sl = dom_mockstyledom.getselectorlist();

  assert(sc.isselectedby(selectorlist(selector(elementtype.circle, 1))));
  assert(sl.isselectedby(selectorlist(selector(elementtype.circle, 1),
                                      selector(elementtype.line, 2))));
}


void test_parents_onedeep_biggertree() {
  circle(50, (0,0));
  circle(50, (0,0));
  circle c = circle(50, (0,0));
  selectorlist sc = dom_mockstyledom.getselectorlist();
  circle(50, (0,0));
  line l = line(unitsquare, c);
  selectorlist sl = dom_mockstyledom.getselectorlist();

  assert(sc.isselectedby(selectorlist(selector(elementtype.circle, 3))));
  assert(sl.isselectedby(selectorlist(selector(elementtype.circle, 3),
                                      selector(elementtype.line, 1))));
}

void test_parents_twodeep() {
  circle c = circle(50, (0,0));
  circle c2 = circle(50, (0,0), c);
  line l = line(unitsquare, c2);
  selectorlist sl = dom_mockstyledom.getselectorlist();

  assert(sl.isselectedby(selectorlist(selector(elementtype.circle, 1),
                                      selector(elementtype.circle, 1),
                                      selector(elementtype.line, 1))));
}
