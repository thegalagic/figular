// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import "figular/styledom" as styledom;
import "figular/element.asy" as element;

/* *********************************
   * TESTS                         *
   ********************************* */

void test_elementtypes_distinct() {
  // This is not complete but you get the idea
  assert(elementtype.textbox != elementtype.line);
  assert(elementtype.circle != elementtype.textbox);
  assert(elementtype.shape != elementtype.textbox);
  assert(elementtype.line != elementtype.textbox);
}

void test_elementequality() {
  line l = line(unitcircle);
  circle c = circle(50, (0,0));
  element e1 = element(l);
  element e2 = element(l);
  element e3 = element(c);
  element e4 = element(circle(50, (0,0)));

  assert(e1 == e2);
  // Check both operators have been defined correctly and are mirrors
  assert(!(e1 != e2));
  // Different element types
  assert(e1 != e3);
  assert(!(e1 == e3));
  // Same element type but different elements
  assert(e3 != e4);
}
