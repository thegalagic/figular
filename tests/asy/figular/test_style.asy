// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import "figular/style" as istyle;

/* *********************************
   * TESTS                         *
   ********************************* */

void test_style_tostring() {
  style s1 = style(border_color=red, font_size=36);
  style s2 = style(border_color=blue,
                   strokeopacity=opacity(0.5),
                   border_width=99,
                   border_style=dashed,
                   join=roundjoin,
                   cap=squarecap,
                   background_color=green,
                   font_family=font.palatino,
                   color=purple,
                   font_size=36);

  assert((string)s1 == "(border_color=[rgb,1,0,0,], opacity=UNSET, border_width=UNSET, border_style=[UNSET], join=UNSET, cap=UNSET, background_color=[gray,0,UNSET], font_family=[UNSET], color=[UNSET], font_size=36)");
  assert((string)s2 == "(border_color=[rgb,0,0,1,], opacity=0.5, border_width=99, border_style=[8,8,0,scale=true,adjust=true,], join=1, cap=0, background_color=[rgb,0,1,0,rgb,0,1,0,], font_family=[\usefont{OT1}{ppl}{m}{n}], color=[rgb,0.5,0,1,], font_size=36)");
}

void test_style_comparison() {
  style s1 = style(border_color=red, font_size=36);
  style s2 = style(border_color=red, font_size=36);

  assert(s1 == s2);
}

void test_styles_add_to_empty_style() {
  style s1 = style();
  style s2 = style(border_color=red,
                   strokeopacity=opacity(0.5),
                   border_width=99,
                   border_style=dashed,
                   join=roundjoin,
                   cap=roundcap,
                   background_color=green,
                   font_family=font.palatino,
                   color=purple,
                   font_size=36);

  style result = s1 + s2;

  assert(result.border_color == red);
  assert(result.strokeopacity == opacity(0.5));
  assert(result.border_width == 99);
  assert(result.border_style == dashed);
  assert(result.join == roundjoin);
  assert(result.cap == roundcap);
  assert(result.background_color == green);
  assert(result.font_family == font.palatino);
  assert(result.color == purple);
  assert(result.font_size == 36);
}

void test_styles_combine_correctly() {
  style s1 = style(border_color=black,
                   strokeopacity=defaultpen,
                   border_width=0,
                   border_style=solid);
  style s2 = style(join=roundjoin,
                   cap=roundcap,
                   background_color=blue,
                   font_family=font.computermodern_roman,
                   color=black,
                   font_size=12pt);
  style combined = style(border_color=black,
                         strokeopacity=defaultpen,
                         border_width=0,
                         border_style=solid,
                         join=roundjoin,
                         cap=roundcap,
                         background_color=blue,
                         font_family=font.computermodern_roman,
                         color=black,
                         font_size=12pt);
  style result = s1 + s2;
  assert(result == combined);
}

void test_style_draw_does_not_abort_if_border_correct() {
  // Note: we cannot test the failure states as asyunit can't cope with aborts yet
  style noborder = style(border_width=0,
                   background_color=red);
  style border = style(border_color=blue,
                   strokeopacity=opacity(1),
                   border_width=1,
                   border_style=solid,
                   join=roundjoin,
                   cap=roundcap,
                   background_color=red);
  page p;
  noborder.filldraw(p, unitsquare);
  border.filldraw(p, unitsquare);
}
