// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

import "figular/selector" as selector;

/* *********************************
   * TESTS                         *
   ********************************* */

void test_selectormatches() {
  selector s1 = selector(elementtype.circle);
  selector s2 = selector(elementtype.circle);
  selector s3 = selector(elementtype.line);
  assert(s1.match(s2));
  assert(!s1.match(s3));

  selector s1 = selector(1);
  selector s2 = selector(1);
  selector s3 = selector(2);
  assert(s1.match(s2));
  assert(!s1.match(s3));

  selector s1 = selector(elementtype.circle);
  selector s2 = selector(1);
  assert(!s1.match(s2));

  selector s1 = selector(elementtype.circle, 1);
  selector s2 = selector(elementtype.circle, 1);
  selector s3 = selector(elementtype.line, 1);
  selector s4 = selector(elementtype.line, 2);
  assert(s1.match(s2));
  assert(!s1.match(s3));
  assert(!s1.match(s4));
  assert(!s3.match(s4));
}

void test_selectorlistempty_matches_all() {
  selectorlist target1 = selectorlist(selector(elementtype.circle, 1));
  selectorlist target2 = selectorlist(selector(elementtype.textbox, 2));
  assert(target1.isselectedby(selectorlist()));
  assert(target2.isselectedby(selectorlist()));
}

void test_selectorlist() {
  selectorlist target = selectorlist(selector(elementtype.circle, 1));

  // By element
  assert(target.isselectedby(selectorlist(selector(elementtype.circle))));
  assert(!target.isselectedby(selectorlist(selector(elementtype.line))));

  // By index
  assert(target.isselectedby(selectorlist(selector(1))));
  assert(!target.isselectedby(selectorlist(selector(2))));

  // By both
  assert(target.isselectedby(selectorlist(selector(elementtype.circle, 1))));

  // One-level deep...
  selectorlist target = selectorlist(selector(elementtype.circle, 1), selector(elementtype.circle, 3));
  // ...fails
  assert(!target.isselectedby(selectorlist(selector(1))));
  assert(!target.isselectedby(selectorlist(selector(elementtype.circle, 1))));
  assert(!target.isselectedby(
            selectorlist(selector(elementtype.circle, 1), selector(elementtype.line, 3))
         ));
  assert(!target.isselectedby(
            selectorlist(selector(elementtype.circle, 1), selector(elementtype.circle, 2))
         ));
  // ...success
  assert(target.isselectedby(
            selectorlist(selector(elementtype.circle, 1), selector(elementtype.circle, 3))
         ));

  // Single selector at top-level matches same element at any depth
  assert(target.isselectedby(selectorlist(selector(elementtype.circle))));
}
