# SPDX-FileCopyrightText: 2021-2 Galagic Limited, et. al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# figular generates visualisations from flexible, reusable parts
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from fastapi.testclient import TestClient
from figular.app import main
from figular.app.main import app
import os
import pytest


client = TestClient(app)
env_var_name = "FIGULAR_SUBPROCESS_TIMEOUT"


def test_timeout_too_high():
    os.environ[env_var_name] = "99"
    with pytest.raises(Exception) as e:
        main.set_subprocess_timeout()

    assert "Invalid subprocess_timeout: 99" in str(e)


def test_timeout_too_low():
    os.environ[env_var_name] = "-99"
    with pytest.raises(Exception) as e:
        main.set_subprocess_timeout()

    assert "Invalid subprocess_timeout: -99" in str(e)


def test_timeout_just_right():
    # Arbitrary value which should always be ok
    os.environ[env_var_name] = "7"
    result = main.set_subprocess_timeout()

    assert result == 7


def test_timeout_not_set_so_defaults():
    os.environ.pop(env_var_name, None)
    result = main.set_subprocess_timeout()

    # Intentionally not specific so default can vary
    assert result > 0 and result < 30


# #############################################################################
# Exercise actual API
# #############################################################################


def test_status():
    r = client.get("/status")
    assert r.status_code == 200
    assert r.json() is None


def test_empty():
    r = client.post("/svg/concept/circle", json={"data": '\r'})
    assert r.status_code == 422  # Unprocessable entity
    assert r.json() == {"detail": "Data insufficient for drawing"}
