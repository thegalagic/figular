# SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# figular generates visualisations from flexible, reusable parts
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from fastapi.testclient import TestClient
from figular import command_line
from figular.app.main import app
from hypothesis import (example, given, HealthCheck, settings,
                        strategies as st)
import pytest
import os
import re
import string

client = TestClient(app)


def contains_content(possible, ignore):
    non_content = list(string.whitespace)
    non_content.append(ignore)
    # Allow spaces and tabs to count as content
    non_content.remove(" ")
    non_content.remove("\t")
    content_count = len(possible)
    for char in non_content:
        content_count -= possible.count(char)
    return content_count


@pytest.fixture()
def cd_tmp_path(tmp_path):
    prev_dir = os.getcwd()
    # tmp_path typically looks like
    # /tmp/pytest-of-user/pytest-N/TESTFUNCTION/
    os.chdir(tmp_path)
    yield tmp_path
    os.chdir(prev_dir)


def test_integration_command_line(cd_tmp_path):
    command_line.main(["org/orgchart", "CEO|Champion",
                       '{ "shape": { "background_color": "pink",'
                       '             "border_width": "0.5" }}'])
    # Simple check for now
    assert os.path.getsize("out.svg") == 6423


def test_integration_api():
    r = client.post(
            "/svg/org/orgchart",
            json={"data": "CEO|Champion"})
    assert r.status_code == 200
    assert r.headers['content-type'] == "image/svg+xml"
    assert len(r.content) == 6081  # simple check for now


@settings(deadline=2000,    # Increase default to give Asy time
          max_examples=3,   # These are slow tests, but always test at least 1
                            # more than provided examples below so we explore
          suppress_health_check=(HealthCheck.function_scoped_fixture,))
@example(output_format={'format': 'png', 'content-type': 'image/png'},
         data='\r')
@example(output_format={'format': 'svg', 'content-type': 'image/svg+xml'},
         data='\r')
@given(st.sampled_from([{"format": "svg",
                         "content-type": "image/svg+xml"},
                        {"format": "png",
                         "content-type": "image/png"}]),
       st.from_regex('[' + re.escape(string.printable) + ']+',
                     fullmatch=True))
def test_integration_api_fuzzing_valid_data(output_format, data):
    # We should fuzz the style also
    r = client.post(f"/{output_format['format']}/org/orgchart",
                    json={"data": data})

    # We can pass anything to the figure, but only content we can use
    # will result in a graphic
    if contains_content(data, ignore="|"):
        assert r.status_code == 200
        assert r.headers['content-type'] == output_format["content-type"]
        assert len(r.content) > 0  # simple check for now
    else:
        assert r.status_code == 422  # Unprocessable entity


@settings(max_examples=10,  # We don't need to labour this
          suppress_health_check=(HealthCheck.function_scoped_fixture,))
@given(st.sampled_from(["svg", "png"]),
       st.from_regex('[^' + re.escape(string.printable) + ']*',
                     fullmatch=True))
def test_integration_api_fuzzing_invalid_data(output_format, data):
    # This is not a great test, just basically testing pydantic is setup
    # ok. We don't check every field either, just data for now.
    r = client.post(f"/{output_format}/org/orgchart",
                    json={"data": data})

    assert r.status_code == 422  # Unprocessable entity
