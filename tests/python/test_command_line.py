# SPDX-FileCopyrightText: 2021-2 Galagic Limited, et. al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# figular generates visualisations from flexible, reusable parts
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from figular import command_line
from os.path import exists
import os
import pytest


@pytest.fixture()
def cd_tmp_path(tmp_path):
    prev_dir = os.getcwd()
    # tmp_path typically looks like
    # /tmp/pytest-of-user/pytest-N/TESTFUNCTION/
    os.chdir(tmp_path)
    yield tmp_path
    os.chdir(prev_dir)


def test_nonexistantfile(cd_tmp_path, capsys):
    with pytest.raises(SystemExit) as e:
        command_line.main(["myfile.asy"])

    out, err = capsys.readouterr()
    assert out == "fig: argument myfile.asy was neither a figure nor a file!\n"
    assert e.value.code == 1


def test_existantfile(cd_tmp_path):
    with open('myfile.asy', "w") as f:
        f.write("void run(picture p, string[] input) { dot(p, (0,0)); }")

    assert command_line.main(["myfile.asy", "data",
                              '{ "circle": { "border_width": 3} }']) == 0
    assert exists("out.svg")


def test_existantfile_error(cd_tmp_path):
    with open('myfile.asy', "w") as f:
        f.write("assert(false, 'asy executed');")

    assert command_line.main(["myfile.asy"]) == 1


def test_help():
    with pytest.raises(SystemExit) as e:
        command_line.main(["--help"])

    assert e.value.code == 0
