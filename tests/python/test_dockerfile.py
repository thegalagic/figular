# SPDX-FileCopyrightText: 2021-2 Galagic Limited, et. al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# figular generates visualisations from flexible, reusable parts
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import json
import os
import pytest
import requests
import subprocess
import time


container_port = "9080"


@pytest.fixture(scope="session")
def container():
    # Use flatpak-spawn trick to run ensure podman runs on host.
    # This may not be required in future:
    # [Issue #145 · containers/toolbox · GitHub]
    # (https://github.com/containers/toolbox/issues/145)
    out = subprocess.check_output(["flatpak-spawn", "--host",
                                   "podman", "build", "-t", "figular",
                                   os.path.abspath(".")], text=True)
    # Image ID in last line but followed by a newline
    image = out.split("\n")[-2]
    cont = subprocess.check_output(["flatpak-spawn", "--host",
                                    "podman", "run", "-d", "-p",
                                    f"{container_port}:8080",
                                    image], text=True).strip()
    # Nasty hard-coded wait. At some point podman may support waiting on
    # 'healthy' [Allow `podman wait` to wait on healthy state]
    # (https://github.com/containers/podman/issues/13627)
    time.sleep(1)
    yield cont

    # Cleanup
    logs = subprocess.check_output(["flatpak-spawn", "--host",
                                    "podman", "logs", cont], text=True,
                                   stderr=subprocess.STDOUT)
    print(logs)
    subprocess.check_output(["flatpak-spawn", "--host",
                             "podman", "stop", cont])


def test_status(container):
    r = requests.get(f"http://localhost:{container_port}/status")

    assert r.status_code == 200
    assert r.json() is None


def test_a_figure(container):
    payload = {'data': 'Hello'}
    r = requests.post(f"http://localhost:{container_port}/svg/concept/circle",
                      data=json.dumps(payload))

    assert r.status_code == 200
    assert r.headers['Content-Type'] == 'image/svg+xml'
    assert len(r.text) == 3702
