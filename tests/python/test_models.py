# SPDX-FileCopyrightText: 2021-2 Galagic Limited, et. al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# figular generates visualisations from flexible, reusable parts
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from figular.models import Message
import pytest
import pydantic


def test_empty():
    msg = Message(data="irrelevant")
    assert msg.getasy() == (";")


def test_extra_rubbish():
    with pytest.raises(pydantic.error_wrappers.ValidationError) as e:
        Message(data="irrelevant", style={"nonsense": "whatever"})
    assert "extra fields not permitted" in str(e.value)


def test_basicstyle():
    msg = Message(data="irrelevant",
                  style={"line": {"border_color": "blue"}})
    assert msg.getasy() == ("dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.line)), "
                            "style(border_color=blue)));")
    msg = Message(data="irrelevant",
                  style={"shape": {"border_color": "blue"}})
    assert msg.getasy() == ("dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.shape)), "
                            "style(border_color=blue)));")
    msg = Message(data="irrelevant",
                  style={"circle": {"border_color": "blue"}})
    assert msg.getasy() == ("dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.circle)), "
                            "style(border_color=blue)));")
    msg = Message(data="irrelevant",
                  style={"textbox": {"font_family": "Computer Modern Roman"}})
    assert msg.getasy() == ("dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.textbox)), "
                            "style(font_family=font.computermodern_roman)));")

    # Font Weights
    msg = Message(data="irrelevant",
                  style={"textbox": {"font_family": "Computer Modern Roman",
                                     "font_weight": "normal"}})
    assert msg.getasy() == ("dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.textbox)), "
                            "style(font_family=font.computermodern_roman)));")
    msg = Message(data="irrelevant",
                  style={"textbox": {"font_family": "Computer Modern Roman",
                                     "font_weight": "bold"}})
    assert msg.getasy() == ("dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.textbox)), "
                            "style(font_family="
                            "font.computermodern_roman_bold)));")


def test_numericalswhenzero():
    # Some properties are suspectible to being missed if they are similar to
    # 'false'
    msg = Message(data="irrelevant",
                  style={"line": {"border_width": "0"}})
    assert msg.getasy() == ("dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.line)), "
                            "style(border_width=0.0)));")
    msg = Message(data="irrelevant",
                  style={"shape": {"border_width": "0"}})
    assert msg.getasy() == ("dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.shape)), "
                            "style(border_width=0.0)));")
    msg = Message(data="irrelevant",
                  style={"textbox": {"font_size": "0"}})
    assert msg.getasy() == ("dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.textbox)), "
                            "style(font_size=0.0)));")
    msg = Message(data="irrelevant",
                  style={"circle": {"border_width": "0"}})
    assert msg.getasy() == ("dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.circle)), "
                            "style(border_width=0.0)));")
    msg = Message(data="irrelevant",
                  style={"figure_concept_circle": {"rotation": "0"}})
    assert msg.getasy() == ("registercirclestyle(degreeStart=0);")


def test_depth_one():
    msg = Message(data="irrelevant",
                  style={"shape": {"circle": {"border_color": "pink"}}})
    assert msg.getasy() == (";dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.shape), "
                            "selector(elementtype.circle)), "
                            "style(border_color=pink)));")


def test_depth_two():
    msg = Message(data="irrelevant",
                  style={"shape": {"circle":
                                   {"shape": {"border_color": "pink"}}}})
    assert msg.getasy() == (";;dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.shape), "
                            "selector(elementtype.circle), "
                            "selector(elementtype.shape)), "
                            "style(border_color=pink)));")


def test_style_at_two_depths():
    msg = Message(data="irrelevant",
                  style={"shape": {"circle": {"border_color": "pink"},
                                   "border_color": "blue"}})
    assert msg.getasy() == ("dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.shape)), "
                            "style(border_color=blue)));"
                            "dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.shape), "
                            "selector(elementtype.circle)), "
                            "style(border_color=pink)));")


def test_nthchild_one():
    msg = Message(data="irrelevant",
                  style={"shape":
                         {"textbox_nth_child_1": {"font_size": "24"}}})
    assert msg.getasy() == (";dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.shape), "
                            "selector(elementtype.textbox, 1)), "
                            "style(font_size=24.0)));")


def test_nthchild_two():
    msg = Message(data="irrelevant",
                  style={"shape":
                         {"textbox_nth_child_2": {"color": "blue"}}})
    assert msg.getasy() == (";dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.shape), "
                            "selector(elementtype.textbox, 2)), "
                            "style(color=blue)));")


def test_nthchild_three():
    msg = Message(data="irrelevant",
                  style={"shape":
                         {"textbox_nth_child_3": {"color": "blue"}}})
    assert msg.getasy() == (";dom_styledom.addrule(stylerule(selectorlist("
                            "selector(elementtype.shape), "
                            "selector(elementtype.textbox, 3)), "
                            "style(color=blue)));")
