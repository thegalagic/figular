# SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# figular generates visualisations from flexible, reusable parts
#
# For full copyright information see the AUTHORS file at the top-level
# directory of this distribution or at
# [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from fastapi import Depends, FastAPI, HTTPException
from fastapi.responses import StreamingResponse
from figular.models import Message, OutputFormatEnum
import importlib.resources
import logging
import os
import subprocess
import tempfile


def set_subprocess_timeout():
    result = 3
    env_var_name = "FIGULAR_SUBPROCESS_TIMEOUT"
    if (env_var_name in os.environ):
        result = int(os.environ[env_var_name])

    if (result < 0 or result > 10):
        raise Exception(f"Invalid subprocess_timeout: {result}")

    return result


app = FastAPI()
logger = logging.getLogger("app")
subprocess_timeout = set_subprocess_timeout()
with importlib.resources.path("figular", "__init__.py") as fig_init:
    fig_dir = fig_init.parent
    os.environ["ASYMPTOTE_DIR"] = str(fig_dir)


def create_temp_dir():
    with tempfile.TemporaryDirectory() as tmpdirname:
        yield tmpdirname


def iter_file(filename):
    with open(filename, mode="rb") as f:
        yield from f


def run_figure(output_format: OutputFormatEnum, figure: str,
               msg: Message, tmpdirname: str):
    """ Run the given figure safely and return a FileResponse """
    (_, filename) = tempfile.mkstemp(dir=tmpdirname)
    output_filename = f'{filename}.{output_format.getext()}'

    input = msg.data if msg else None
    cmdprefix = msg.getasy() if msg else ""

    cmd = ['asy',
           '-autoimport',
           os.path.join(fig_dir, figure),
           '-c', f'{cmdprefix} run(currentpicture, input(comment=""));',
           '-safe', '-noView', '-noglobalread',
           '-f', output_format, '-tex', 'xelatex',
           '-o', output_filename
           ]

    try:
        subprocess.run(cmd, input=input,
                       capture_output=True,
                       text=True, check=True,
                       timeout=subprocess_timeout)
        # [Using StreamingResponse with file-like objects]
        # (https://fastapi.tiangolo.com/advanced/custom-response/#using-streamingresponse-with-file-like-objects)
        if (os.path.exists(output_filename)):
            return StreamingResponse(iter_file(output_filename),
                                     media_type=output_format.getmime())
        else:
            raise HTTPException(status_code=422,
                                detail="Data insufficient for drawing")
    except subprocess.CalledProcessError as cpe:
        if cpe.stdout:
            logger.error(cpe.stdout)
        if cpe.stderr:
            logger.error(cpe.stderr)
        raise
    except Exception as e:
        logger.error(e)
        raise


@app.get("/status")
def status():
    """ Basic health check """
    return None


@app.post("/{output_format}/concept/circle")
def asy_circle(output_format: OutputFormatEnum,
               message: Message,
               tmpdirname: str = Depends(create_temp_dir)):
    """ Run the figure, return a streamed file """
    return run_figure(output_format, "concept/circle.asy", message, tmpdirname)


@app.post("/{output_format}/org/orgchart")
def asy_orgchart(output_format: OutputFormatEnum,
                 message: Message,
                 tmpdirname: str = Depends(create_temp_dir)):
    """ Run the figure, return a streamed file """
    return run_figure(output_format, "org/orgchart.asy", message, tmpdirname)


@app.post("/{output_format}/time/timeline")
def asy_timeline(output_format: OutputFormatEnum,
                 message: Message,
                 tmpdirname: str = Depends(create_temp_dir)):
    """ Run the figure, return a streamed file """
    return run_figure(output_format, "time/timeline.asy", message, tmpdirname)
