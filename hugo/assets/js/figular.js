// SPDX-FileCopyrightText: 2021-2 Galagic Limited, et al. <https://galagic.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// figular generates visualisations from flexible, reusable parts
//
// For full copyright information see the AUTHORS file at the top-level
// directory of this distribution or at
// [AUTHORS](https://gitlab.com/thegalagic/figular/AUTHORS.md)
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU Affero General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.

/* This is how we load our Hugo configuration into JS */
/* [JavaScript Building | Hugo](https://gohugo.io/hugo-pipes/js/) */
import * as params from '@params'

function transTabToPipe (str) {
  return str.replaceAll('\t', '|')
}

function showElement (element) {
  element.classList.remove('d-none')
}

function hideElement (element) {
  element.classList.add('d-none')
}

function clearErrors () {
  const alerts = document.getElementById('divResult').getElementsByClassName('alert')
  for (const alertDiv of alerts) {
    hideElement(alertDiv)
  }
}

function setBadResult (div) {
  hideElement(document.getElementById('resultImg'))
  clearErrors()
  showElement(div)
}

function setResult (objectURL, mimeType, downloadName) {
  const resultImg = document.getElementById('resultImg')
  const downloadLink = document.getElementById('downloadLink')
  clearErrors()
  resultImg.src = objectURL
  showElement(resultImg)
  downloadLink.href = resultImg.src
  downloadLink.download = downloadName
  downloadLink.type = mimeType
}

function showSpinner () {
  const spinner = document.getElementById('spinner')
  showElement(spinner)
  spinner.classList.add('d-flex')
}

function hideSpinner () {
  const spinner = document.getElementById('spinner')
  hideElement(spinner)
  spinner.classList.remove('d-flex')
}

function disableFormatButtons () {
  const outputFormatPNG = document.getElementById('output_format_png')
  const outputFormatSVG = document.getElementById('output_format_svg')
  outputFormatPNG.setAttribute('disabled', '')
  outputFormatSVG.setAttribute('disabled', '')
}

function getStatus () {
  const xhttp = new XMLHttpRequest()
  const servicestatus = document.getElementById('servicestatus')
  xhttp.onreadystatechange = function () {
    if (this.readyState === 4 && this.status !== 200) {
      showElement(servicestatus)
      disableFormatButtons()
      serviceDown = true
    }
  }
  xhttp.open('GET', `${params.domain}/status`, true) // eslint-disable-line no-undef
  xhttp.timeout = 1000
  xhttp.send()
}

function logProblem (status) {
  const xhttp = new XMLHttpRequest()
  xhttp.open('GET', `/problems/${figureURL}/` + encodeURIComponent(status.toString()), true)
  xhttp.timeout = 5000
  xhttp.send()
}

function submitDiagram () {
  const xhttp = new XMLHttpRequest()
  const statusFigureDataLarge = document.getElementById('status_figure_data_large')
  const statusFigureDataBad = document.getElementById('status_figure_data_bad')
  const statusFigureDataOther = document.getElementById('status_figure_data_other')
  xhttp.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
      outputFormat.processRequest(this)
      hideSpinner()
    } else if (this.readyState === 4 && this.status === 413) {
      hideSpinner()
      setBadResult(statusFigureDataLarge)
      logProblem(this.status)
    } else if (this.readyState === 4 && this.status === 422) {
      hideSpinner()
      setBadResult(statusFigureDataBad)
      logProblem(this.status)
    } else if (this.readyState === 4 && this.status !== 200) {
      hideSpinner()
      setBadResult(statusFigureDataOther)
      logProblem(this.status)
    } else if (this.readyState !== 4) {
      showSpinner()
    }
  }
  outputFormat.craftRequest(xhttp)
  xhttp.timeout = params.figure_timeout
  xhttp.setRequestHeader('Content-Type', 'application/json')
  try {
    const data = {}
    for (const i of document.querySelectorAll('[data-figular]')) {
      const spec = i.getAttribute('data-figular')
      const transform = i.getAttribute('data-figular-transform')
      let deeper = data
      let parent = null
      let part = null
      for (part of spec.split('.')) {
        if (!Object.hasOwn(deeper, part)) {
          deeper[part] = {}
        }
        parent = deeper
        deeper = deeper[part]
      }
      let finalValue
      if (i.type === 'checkbox') {
        finalValue = i.checked
      } else {
        finalValue = i.value
      }
      // TODO: case/select style here. we're whitelisting allowed functions
      if (transform === 'tabtopipe') {
        finalValue = transTabToPipe(finalValue)
      }
      parent[part] = finalValue
    }
    xhttp.send(JSON.stringify(data))
  } catch (error) {
    logProblem(error)
  }
}

function markChange () {
  stampChange = Date.now()
}

function submitOnChange () {
  if (stampChange && document.querySelector('[data-figular=data]').value.length > 0) {
    if (!serviceDown && (Date.now() - stampChange) > 1000) {
      stampChange = null
      submitDiagram()
    }
  }
}

function OutputFormat (onchange) {
  this.lastObjectURL = null
  this.png = true
  this.onchange = onchange
  this.toggleFormat = function () {
    this.png = !this.png
    this.onchange()
  }
  this.craftRequest = function (xhttp) {
    if (this.png) {
      xhttp.open('POST', `${params.domain}/png/${figureURL}`, true) // eslint-disable-line no-undef
      // This is for any binary format really
      xhttp.responseType = 'blob'
    } else {
      // Only for SVG format
      xhttp.open('POST', `${params.domain}/svg/${figureURL}`, true) // eslint-disable-line no-undef
      xhttp.responseType = 'text'
    }
  }
  this.processRequest = function (xhttp) {
    let newObjectURL
    // TODO: Check this results in non-zero string or have a fallback
    const saveFilename = document.querySelector('section#figular h1').innerText.replace(/[^a-z0-9]/gi, '_').toLowerCase()

    if (this.png) {
      // https://stackoverflow.com/a/13959397/28170
      const blob = new Blob([xhttp.response], { type: 'image/png' })
      newObjectURL = URL.createObjectURL(blob)
      setResult(newObjectURL, 'image/png', saveFilename + '.png')
    } else {
      const blob = new Blob([xhttp.responseText], { type: 'image/svg+xml;charset=utf-8' })
      newObjectURL = URL.createObjectURL(blob)
      setResult(newObjectURL, 'image/svg+xml', saveFilename + '.svg')
    }
    // https://www.bennadel.com/blog/3472-downloading-text-using-blobs-url-createobjecturl-and-the-anchor-download-attribute-in-javascript.htm
    if (this.lastObjectURL) {
      URL.revokeObjectURL(this.lastObjectURL)
      this.lastObjectURL = newObjectURL
    }
  }
}

const outputFormat = new OutputFormat(submitDiagram)
const figureURL = document.querySelector('form[data-figular]').getAttribute('data-figular')

// Watch for changes
let stampChange = null
let serviceDown = false
setInterval(submitOnChange, 1000)

// Hook up events
for (const i of document.querySelectorAll('[data-figular]')) {
  i.oninput = markChange

  // Sibling inputs are each other's mirror, for range input by text and widget
  const sib = i.nextElementSibling
  if (sib && sib.tagName === 'INPUT') {
    i.oninput = function () {
      sib.value = this.value
      markChange()
    }
    sib.oninput = function () {
      i.value = this.value
      markChange()
    }
  }
}

document.getElementById('output_format_png').oninput = function () { outputFormat.toggleFormat() }
document.getElementById('output_format_svg').oninput = function () { outputFormat.toggleFormat() }
getStatus()
